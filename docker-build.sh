#!/bin/sh

# Update base image
docker pull ubuntu:disco

# Build images
docker build -t gitlab-runner/latex:latest latex-build-environment
docker build -t gitlab-runner/owl:latest owl-build-environment
docker build -t gitlab-runner/prism:latest prism-build-environment
docker build -t gitlab-runner/website:latest website-build-environment
docker build -t gitlab-runner/haskell:latest haskell-build-environment

# Remove unused images, replace with system prune if available.
docker rmi $(docker images | grep "<none>" | awk "{print \$3}")
